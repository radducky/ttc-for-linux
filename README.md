# Tamriel Trade Centre for Linux


A tiny script for updating the [TTC](https://tamrieltradecentre.com) price tables in your ESO client.

## How to use

1. Download ttc_updater.sh
2. Open the script and change next params: 
```
ESO_DATA_DIR=
SERVERS=
CLIENTS=
```
3. Run the script each time before starting the game:
```
sh ttc_updater.sh
```

## How to upload your data

For uploading data collected by the addon use TTC web clients here:
- [ ] [EU](https://eu.tamrieltradecentre.com/pc/Trade/WebClient)
- [ ] [NA](https://us.tamrieltradecentre.com/pc/Trade/WebClient)
