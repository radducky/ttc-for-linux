#!/bin/sh
### This script downloads and updates the TTC price tables for the ESO PC client.
# https://gitlab.com/radducky/ttc-for-linux
### Tamriel Trade Centre addon by Steven Chen:
# https://tamrieltradecentre.com
### For uploading data collected by the addon use TTC web clients here:
# EU: https://eu.tamrieltradecentre.com/pc/Trade/WebClient
# NA: https://us.tamrieltradecentre.com/pc/Trade/WebClient

### CUSTOMIZATION
# ESO data directory path
ESO_DATA_DIR="${HOME}/.steam/steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/Documents/Elder Scrolls Online"
# Server: EU, NA, or both
# You can set one server:  SERVERS="eu"
#            or set both:  SERVERS="eu us"
SERVERS="eu"
# ESO client type: LIVE, PTS, or both
# You can set one client:  CLIENTS="live"
#            or set both:  CLIENTS="live pts"
CLIENTS="live"

### SCRIPT
script_path=$(dirname "$(readlink -f "$0")") 
color="\e[1;33m"
no_color="\e[0m"
for server in $SERVERS
do
    echo "${color}Download price table for [${server}]${no_color}"
    wget -O "${script_path}/${server}_pricetable.zip" "https://${server}.tamrieltradecentre.com/download/PriceTable"
    echo "${color}Extract${no_color}"
    for client in $CLIENTS
    do
        if [ -d "${ESO_DATA_DIR}/${client}/AddOns/TamrielTradeCentre" ]
        then
            unzip -o "${script_path}/${server}_pricetable.zip" -d "${ESO_DATA_DIR}/${client}/AddOns/TamrielTradeCentre"
        fi
    done
    echo "${color}Delete price table file for [${server}]${no_color}"
    rm "${script_path}/${server}_pricetable.zip"
    echo "---"
done
echo "${color}Done.${no_color}"
sleep 3
